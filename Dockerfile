from ubuntu
RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get --no-install-recommends install alien -y
RUN apt-get install python3 -y
RUN apt install python3-pip -y
RUN apt-get install sshpass -y
WORKDIR /code
COPY . .
RUN alien -i install_now/wmic-4.0.0tp4-0.x86_64.rpm
RUN pip3 install -r requirements.txt
RUN cp install_now/wrapper.py /usr/local/lib/python3.8/dist-packages/wmi_client_wrapper/
WORKDIR /code/app/
#RUN cd app && flask run


