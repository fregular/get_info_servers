from flask import Blueprint, jsonify, request
import json
import threading_api

sshpass = Blueprint('sshpass', __name__)

@sshpass.route('/sshpass', methods = ['POST'])
def index():
    data = json.loads(request.data)

    if type(data['prefix']) is list:
        thread_init = threading_api.Before_Threading(data['prefix'], 'ssh.get_access')
        result = thread_init.init_threading()
        return jsonify(result)
    else:
        prefix_list = []
        prefix_list.append(data['prefix'])
        thread_init = threading_api.Before_Threading(prefix_list, 'ssh.get_access')
        result = thread_init.init_threading()
        return jsonify(result)
