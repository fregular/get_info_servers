from flask import Flask, jsonify
from sshpass import sshpass
from wmi import wmi
import unittest

app = Flask(__name__)

app.register_blueprint(sshpass)
app.register_blueprint(wmi)

@app.cli.command()
def test():
    test = unittest.TestLoader().discover('test')
    unittest.TextTestRunner().run(test)

@app.route('/')
def hello():
    return "Hello world"
    
