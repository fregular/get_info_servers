from flask import Blueprint, jsonify, request
import json
import winrun
import threading_api

wmi = Blueprint('wmi', __name__)

@wmi.route('/wmi', methods=['POST'])
def index():
    data = json.loads(request.data)
    win = winrun.Winmaprun()
    result = []
    if type(data['prefix']) is list:
        thread_init = threading_api.Before_Threading(data['prefix'], 'win.process_ip')
        result = thread_init.init_threading()
    else:
        prefix_list = []
        prefix_list.append(data['prefix'])
        thread_init = threading_api.Before_Threading(prefix_list, 'win.process_ip')
        result = thread_init.init_threading()
    
    return jsonify(result)
