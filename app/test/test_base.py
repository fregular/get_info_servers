from flask_testing import TestCase
from app import app
import requests

class MainTest(TestCase):
    def create_app(self):
        app.config['TESTING'] = True
        return app

    def test_flask_run(self):
        response = requests.get('http://127.0.0.1:5000/')
        self.assertEqual(response.status_code, 200)
        

    def test_flask_hello(self):
        response = requests.get('http://127.0.0.1:5000/')
        self.assertEqual(response.text, "Hello world")

