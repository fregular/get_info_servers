SUNOS="$(uname)";
if [ "$SUNOS" == "SunOS" ]; then
    R_HOST=$(hostname);
    SO_VERSION=$(cat /etc/release|head -1);
elif which oslevel 1>/dev/null 2>&1 && [ "$SUNOS" != "SunOS" ]; then
    MEMORY=$(prtconf -m|awk "{print \$3}");
    MEMORY_SWAP=$(lsps -s|grep -v Total|awk "{print \$1}"|sed 's/MB//g');
    NETWORK_INTERFACE=$(lsdev -Cc if|grep Available|awk "{print \$1}");
    OPERATION_SYSTEM=$(uname);
    #CORE=$(sudo smtctl | grep -i "This system su"|awk "{print \$6}");
    #CORE=$(sudo smtctl|grep "has"|awk "{print \$3}"|head -1);
    CORE_COUNT=$(lsdev -Cc processor|grep Available |wc -l|awk "{print \$1}");
    PROC=$(prtconf | grep -i "Processor Type"|awk "{print \$3}");
    DISTRIBUTION=$(uname);
    DISTRIBUTION_VERSION=$(oslevel);
    ARCH_VERSION=$(uname -p);
    CAPPED=$(lparstat|grep mode|awk "{print \$4}"|cut -d= -f2)
    ENT=$(lparstat|grep "ent="|awk "{print \$9}"|cut -d= -f2)
    KERNEL=$(ls -l /unix |awk "{print \$11}");
    UPTIME=$(uptime);
    SO_INSTALL_DATE=$(lslpp -h bos.rte|grep COMPLETE|head -1|awk "{print \$4}")
    LAST_LOGON=$(last|head -3)

    R_HOST=$(hostname -s);
    SO_VERSION="AIX $(oslevel)";
    TYPE_SERVER=$(uname -L);
    MANUFACTURER=$(uname -M|cut -d, -f1)
    DC_AUTH_SOFTWARE=$( if [[ -z $(sudo adinfo) ]]; then echo "none"; else echo "centrify"; fi )
    DC_AUTH_STATUS=$(if [[ $(sudo adinfo |grep "CentrifyDC" |awk "{print \$3}") == "connected" ]]; then echo "connected"; else echo "disconnected";fi)

    if [ "$DC_AUTH_STATUS" == "connected" ]; then
	    DOMAIN=$DOMAIN
    fi;
    
    v_maxage="cat /etc/login.defs | grep -v '^#' | grep PASS_MAX_DAYS| awk '{print \$2}'"
[ -z "$(eval "$v_maxage")" ] && v_maxage="echo 99999"


    
    if [ x != x"$(lslpp -Lc|grep -i ibm|egrep -i "websphere|tivoli|db2|ihs")" ]; then
      IBM_PRODUCT="True";
    else
      IBM_PRODUCT="False";
    fi;
    if [ "$TYPE_SERVER" != "-1 NULL" ]; then
      TYPE_SERVER="Virtual";
    else
      TYPE_SERVER="Physical";
    fi;
    if [ x != x"$(lslpp -Lc|grep -i "java"|cut -d: -f 1,2,3,8)" ]; then
      JAVA="True";
      JAVA_VERSION=$(lslpp -Lc|grep -i "java"|cut -d: -f 1,2,3,8)
    else
      JAVA="False";
    fi;
    if [ "$SO_VERSION" == "AIX 5.3.0.0" ]; then
      CORE=$(sudo smtctl |grep "has"|awk "{print \$3}"|head -1);
    else
      CORE=$(sudo smtctl|grep "This system supports"|awk "{print \$6}")
    fi;
    LOGICALPROCESSORS=$( expr $CORE \* $CORE_COUNT )

    SYSTEM_MODEL=$(prtconf|grep "System Model"|awk "{print \$3}")
    MACHINE_SERIAL_NUMBER=$(prtconf|grep "Machine Serial Number"|awk "{print \$4}")
    WAS_INSTALLED=$(lslpp -L all|grep WebSphere)
    if [ $? == 0 ]; then
      WAS_INSTALLED="True"
      WAS=$(lslpp -L all|grep WebSphere|grep IBM|head -1|awk "{print \$5\" \"\$6\" \"\$7\" \"\$8}")
      WAS_VERSION=$(lslpp -L all|grep WebSphere|grep IBM|head -1| awk "{print \$2}")
    else
      WAS_INSTALLED="False";
    fi;

else
    MEMORY=$(free -m|grep Mem|awk "{print \$2}")
    MEMORY_SWAP=$(free -m|grep Swap|awk "{print \$2}")
    NETWORK_INTERFACE=$(ls /sys/class/net/)
    OPERATION_SYSTEM=$(uname)
    CORE_COUNT=$(grep '^physical id' /proc/cpuinfo |sort -u|wc -l)
    #CORE=$(grep "^cpu cores" /proc/cpuinfo |sort -u|wc -l)
    CORE=$(grep "^cpu cores" /proc/cpuinfo |sort -u|awk "{print \$4}");
    LOGICALPROCESSORS=$(grep processor /proc/cpuinfo |wc -l)
    if [ "$CORE_COUNT" == 0 ]; then
      CORE_COUNT=$(grep ^processor /proc/cpuinfo | wc -l);
      CORE="1"
    fi;
    PROC=$(cat /proc/cpuinfo |grep "model name"|sort -u|cut -d: -f2|sed -e 's/^[[:space:]]*//');
    DISTRIBUTION=$(lsb_release -is 2>/dev/null || cat /etc/[Ssr]*-release | head -n1 |awk "{print \$1 \" \"\$2}");
    DISTRIBUTION_VERSION=$(lsb_release -rs 2>/dev/null || cat /etc/[Ssr]*-release|grep "VERSION" |awk "{print \$3}");
    ARCH_VERSION=$(uname -p);
    KERNEL=$(uname -r);
    UPTIME=$(uptime);

    LAST_LOGON=$(last|head -3)
    R_HOST=$(hostname -s);
    #SO_VERSION=$([ -f /etc/redhat-release ] && cat /etc/redhat-release || cat /etc/release);
    #SO_VERSION=$(lsb_release -ds 2>/dev/null || cat /etc/*release 2>/dev/null | head -n1 || uname -om)
    SO_VERSION=$(lsb_release -ds 2>/dev/null || cat /etc/[Ssr]*-release 2>/dev/null | head -n1)
    #SO_VERSION=$(lsb_release -ds 2>/dev/null || cat /etc/*release 2>/dev/null | head -n1 || uname -om)
    #TYPE_SERVER=$(sudo dmidecode -s system-manufacturer 2>&1);
    #TYPE_SERVER=$(sudo dmidecode -s system-manufacturer 2>&1);
    #TYPE_SERVER=$(sudo dmidecode -s system-manufacturer 2>/dev/null|| sudo dmidecode |grep -A2 "System Infor"|grep Manufac|awk "{print \$2 \" \" \$3}");
    #TYPE_SERVER=$(sudo -i dmidecode -s system-manufacturer 2>/dev/null|grep -v ^\#  || sudo dmidecode |grep -A2 "System Infor"|grep Manufac|awk "{print \$2 \" \" \$3}");
    TYPE_SERVER=$(sudo -i dmidecode -s system-manufacturer 2>/dev/null|tail -1  || sudo dmidecode |grep -A2 "System Infor"|grep Manufac|awk "{print \$2 \" \" \$3}");
    SO_INSTALL_DATE=$(rpm -qa --last | tail -1|cut -d " " -f 15-60)

    DC_AUTH_SOFTWARE=$( if [[ $(sudo systemctl status winbind 2>/dev/null|grep "Active"|awk "{print \$3}") == "(running)" || $(sudo /etc/init.d/winbind status 2>/dev/null|cut -d"(" -f2|awk "{print \$1}") == "pid" ]]; then echo "winbind"; else if [[ $(sudo adinfo 2>/dev/null) ]]; then echo "centrify"; else echo "none"; fi ; fi )


    DC_AUTH_STATUS=$( if [[ $(sudo systemctl status winbind 2>/dev/null|grep "Active"|awk "{print \$3}") == "(running)" && $(sudo cat /etc/samba/smb.conf|grep "realm"|grep $DOMAIN|awk "{print \$3}") == $DOMAIN || $(sudo /etc/init.d/winbind status 2>/dev/null|cut -d"(" -f2|awk "{print \$1}") == "pid" && $(sudo cat /etc/samba/smb.conf|grep "realm"|grep $DOMAIN |awk "{print \$3}") == $DOMAIN || $(sudo adinfo |grep "CentrifyDC"|awk "{print \$3}") == "connected" ]]; then echo "connected"; else echo disconnected; fi )

    if [ "$DC_AUTH_STATUS" == "connected" ]; then
	    DOMAIN=$DOMAIN
	    else
		    DOMAIN="none"
    fi;


    #if [ x != x"$(sudo find  / /opt /home /var /u* -xdev ! -fstype nfs ! -fstype cifs ! -fstype ntfs -type d -iname 'IBM' ! -empty 2>&1)" ]; then
    #if [ x != x"$(sudo find  / /opt /home /var /u* -xdev ! -fstype nfs ! -fstype cifs ! -fstype ntfs ! -fstype tempfs ! -fstype proc  ! -fstype sysfs  -type d -iname 'IBM' ! -empty)" ]; then
    #  IBM_PRODUCT="True";
    #else
    #  IBM_PRODUCT="False";
    #fi;
    if [ "$TYPE_SERVER" == "VMware, Inc." ] || [ "$TYPE_SERVER" == "Xen"  ]; then
      MANUFACTURER=$TYPE_SERVER
      TYPE_SERVER="Virtual";

    else
      if [ "$TYPE_SERVER" == "" ]; then
	MANUFACTURER=""
	TYPE_SERVER=""
      else
        MANUFACTURER=$TYPE_SERVER
        TYPE_SERVER="Physical";
    fi;
    fi;

    DC_AUTH_SOFTWARE=$( if [[ -z $(sudo adinfo) ]]; then echo "none"; else echo "centrify"; fi )


    #SUDO=$( if sudo grep admcmdb /etc/sudoers|grep NOPASSWD|grep -v "#" 1>/dev/null 2>/dev/null ||sudo ls /etc/sudoers.d/admcmdb 1>/dev/null 2>/dev/null || sudo grep wheel /etc/group|grep admcmdb|sudo grep wheel /etc/sudoers|grep -v "#" 1>/dev/null 2>/dev/null; then echo "True"; else echo "False";fi )
#    SUDO=$(if sudo grep admcmdb /etc/sudoers|grep NOPASSWD|grep -v "#" 1>/dev/null 2>/dev/null ||sudo ls /etc/sudoers.d/admcmdb 1>/dev/null 2>/dev/null || id admcmdb|grep wheel|sudo grep wheel /etc/sudoers|grep -v "#" 1>/dev/null 2>/dev/null; then echo "True"; else echo "False";fi)
    SUDO=$(if sudo cat /etc/sudoers 1>/dev/null 2>/dev/null; then echo "True"; else echo "False"; fi)

    

    #JAVA_VERSION=$(sudo find / -name "java" -not -path "./dev/*" -not -path "./var/*" -not -path "./home/*" -not -path "./lib/*" -not -path "./lib64/*" -not -path "./bin/*" -not -path "./boot/*" -not -path "./media/*" -not -path "./mnt/*" -not -path "./proc/*" -not -path "./sbin/*"  ! -fstype nfs ! -fstype cifs ! -fstype ntfs ! -fstype tempfs ! -fstype proc  ! -fstype sysfs -type f -perm -u+x  ! -empty | awk '{print $1" -version"}' | sh -x | xargs echo -n)
    #JAVA_VERSION=$(sudo find / -name "java" -not -path "./dev/*" -not -path "./var/*" -not -path "./home/*" -not -path "./lib/*" -not -path "./lib64/*" -not -path "./bin/*" -not -path "./boot/*" -not -path "./media/*" -not -path "./mnt/*" -not -path "./proc/*" -not -path "./sbin/*"  ! -fstype nfs ! -fstype cifs ! -fstype ntfs ! -fstype tempfs ! -fstype proc  ! -fstype sysfs -type f -perm -u+x  ! -empty -exec echo {}  \; -exec  {} -version 2>&1\;)
    #JAVA_VERSION=$(sudo find / -name "java" -not -path "./dev/*" -not -path "./var/*" -not -path "./home/*" -not -path "./lib/*" -not -path "./lib64/*" -not -path "./bin/*" -not -path "./boot/*" -not -path "./media/*" -not -path "./mnt/*" -not -path "./proc/*" -not -path "./sbin/*"  ! -fstype nfs ! -fstype cifs ! -fstype ntfs ! -fstype tempfs ! -fstype proc  ! -fstype sysfs -type f -perm -u+x  ! -empty -exec echo {}"=" \; -exec  {} -version 2>&1 \;)
    #JAVA_VERSION=$(sudo find / -name "java" -not -path "./dev/*" -not -path "./var/*" -not -path "./home/*" -not -path "./lib/*" -not -path "./lib64/*" -not -path "./bin/*" -not -path "./boot/*" -not -path "./media/*" -not -path "./mnt/*" -not -path "./proc/*" -not -path "./sbin/*"  ! -fstype nfs ! -fstype cifs ! -fstype ntfs ! -fstype tempfs ! -fstype proc  ! -fstype sysfs -type f -perm -u+x  ! -empty -exec echo {}"=" \; -exec  {} -version 2>&1 \;)
    #if [ $? -eq 0 ]; then
    #  JAVA="True";
    #else
    #  JAVA="False";
    #fi;
fi;

if ! PYTHON=$(python2.6 -V 2>&1); then
    if ! PYTHON=$(python -V 2>&1 | egrep "([3][.]|[2][.][6789])" | grep -v grep) ; then
        PYTHON="python not-found";
    fi;
fi;
if DB_MOTOR=$(ps -fea | grep db2sysc |egrep -v grep 2>&1) ; then
    DB_MOTOR="DB2"
elif [ -f "/etc/oratab" 2>&1 ]; then
    DB_MOTOR="Oracle";
elif DB=$(mysql -V 2>&1); then
    DB_MOTOR="Mysql"
elif DB=$(postgres -V 2>&1); then
    DB_MOTOR="Postgres"
else
    DB_MOTOR="no";
fi;
echo $R_HOST;
echo $PYTHON;
echo $SO_VERSION;
echo $DB_MOTOR;
#echo $IBM_PRODUCT;
echo $TYPE_SERVER;
echo $MEMORY;
echo $MEMORY_SWAP;
echo $NETWORK_INTERFACE;
echo $OPERATION_SYSTEM;
echo $CORE;
echo $CORE_COUNT;
echo $PROC;
echo $DISTRIBUTION;
echo $DISTRIBUTION_VERSION;
echo $ARCH_VERSION;
echo $KERNEL;
echo $UPTIME;
echo $CAPPED;
echo $ENT;
echo $SO_INSTALL_DATE;
echo $LAST_LOGON;
echo $MANUFACTURER;
echo $DC_AUTH_STATUS;
echo $DC_AUTH_SOFTWARE;
echo $DOMAIN;
echo $LOGICALPROCESSORS;
echo $SUDO;
echo $SYSTEM_MODEL;
echo $MACHINE_SERIAL_NUMBER;
echo $WAS_INSTALLED;
echo $WAS;
echo $WAS_VERSION;
