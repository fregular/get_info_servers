from subprocess import STDOUT, CalledProcessError, check_output as qx
import os, json, argparse, sys, time, re, datetime
import socket, subprocess, logging



path = 'scripts/script_sshpass.sh'
REDHAT_MAJOR_VERSION = int(os.getenv('REDHAT_MAJOR_VERSION', '6'))
SSHPASS_USER = os.getenv('ANSIBLE_USER','testuser').split(',')
SSHPASS_PASS = os.getenv('ANSIBLE_PASS','testpass').split(',')

class Sshpassrun():
    
    def __init__(self):
        pass

    def get_access(self, ip):
        result = {
            'parsed': 3,
            'err': 'not analyzed',
            'ip': ip,
            'map_type': 'sshpass'
        }
    
        # get ssh user and pass
        accessmode=False
        #host_ip = host['_source']['ip']
    
        try:
            sock = socket.create_connection((ip, 22), timeout=10)
            banner = sock.recv(1024)
        except:
            result['err'] = 'socket err'
            sock = None
            banner = 'banner-decode-err'        
        if(sock):
            try:
                banner = banner.decode()
                banner = banner.lower()
                result['banner'] = banner
            except:
                banner = 'banner-decode-err'
                
            if banner.find("banner-decode") != -1:
                pass
            elif banner.find("microsoft") != -1 or banner.find("remotelyanywhere") != -1:
                result['parsed'] = "-3"
                result['err'] = "Windows"
            elif banner.find("cisco") != -1:
                result['map_type'] = "network"
                result['err'] = "cisco"
            elif banner.find("openssh_3.4p1.rl") != -1:
                result['map_type'] = "network"
                result['err'] = "alcatel"
            elif banner.find("openssh") != -1 or banner.find("sun_ssh") != -1:
                script = open(path, 'r')
                command = script.read()

                listpass = 0
                n = 0
                for sshpass_user in SSHPASS_USER:
                    sshpass = "sshpass -p %s ssh -o StrictHostKeyChecking=no -p %s %s@%s '%s'" % (SSHPASS_PASS[listpass],22,sshpass_user, ip, command)

                #sshpass = "sshpass -p %s ssh -o StrictHostKeyChecking=no -p %s %s@%s '%s'" % (SSHPASS_PASS,22,SSHPASS_USER, ip, command)
                    listpass+=1
                    pipeok = None            
                    try:
                        pipe = subprocess.run(sshpass, shell=True,stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=30)

                        if pipe.returncode == 5 and n == 0:
                            n+=1
                            continue
                        pipeok = True
    
                    except subprocess.TimeoutExpired as err:
                        # timeout
                        result['parsed'] = "-1"
                        result['err'] = 'Timeout'
                    except socket.timeout as err:
                        result['parsed'] = "-1"
                        result['err'] = 'Timeout'
                    except socket.error as err:
                        # error 
                        result['parsed'] = "-2"
                        result['err'] = 'socket err'
                    except:
                        # failed        
                        result['parsed'] = "-3"
                        result['err'] = 'Random'
    
                    if pipeok:
                        result = self.parse_pipe(result, pipe)
                        result['sshpass_user'] = sshpass_user
                        break

            else:
                result['map_type'] = "network"
                result['err'] = "other"
        
        return result

    def parse_pipe(self, result, pipe):
        #print(pipe)

        if(pipe.returncode == 0):
                
            salida = pipe.stdout.decode()
            salida = salida.split("\n")
            result['parsed'] = "-5"
    
            result['hostname'] = salida[0]
            result['ssh_python'] = salida[1]
            result['os_version'] = salida[2]
            result['map_type'] = 'sshpass'
    
            if(salida[1].find('not-found') != -1):
                #Version de python incorrecta
                result['err'] = "old python version"
            else:
                #version de pytohn correcta
                result['err'] = "ready to ansible"
            
            if(salida[3] != "no"):
                result['database_motor'] = salida[3]
                result["database"] = 1
                # result['ssh_SOversion']
            
            #if(salida[4] != "False"):
            #    result['ibm_product'] = True
            #    print(result['ibm_product'])

            #if(salida[5] == "Virtual"):
            #    result['server_type'] = "Virtual"
            #else:
            #    result['server_type'] = "Physical"
            #    print(result['server_type'])
            #if salida[6] == "True":
            #    result['java'] = True
            #    result['java_version'] = salida[7]


            #if(salida[4] == "Virtual"):
            #    result['server_type'] = "Virtual"
            #else:
            #    result['server_type'] = "Physical"
            #    print(result['server_type'])
            result['server_type'] = salida[4]
            result['memory'] = salida[5]
            result['memory_swap'] = salida[6]
            result['network_interface'] = salida[7]
            result['operation_system'] = salida[8]
            result['cores'] = salida[9]
            result['cores_counts'] = salida[10]
            result['proc'] = salida[11]


            ##testing##
            if salida[12] == "SUSE Linux":
                result['distribution'] = salida[12].upper()
            else:
                result['distribution'] = salida[12]

            result['distribution_version'] = salida[13]
            result['arch_version'] = salida[14]
            result['kernel'] = salida[15]
            result['uptime_sshpass'] = salida[16]
            result['capped_or_uncapped'] = salida[17]
            result['ent'] = salida[18]
            result['so_install_date'] = salida[19]
            result['last_logon'] = salida[20]
            result['Manufacturer'] = salida[21]
            result['dc_auth_status'] = salida[22]
            result['dc_auth_software'] = salida[23]
            result['Domain'] = salida[24]
            result['NumberOfLogicalProcessors'] = salida[25]
            #result['sudo'] = bool(salida[26])
            result['sudo'] = salida[26]
            result['system_model'] = salida[27]
            result['machine_serial_number'] = salida[28]
            if salida[29] == "True":
                result['was_installed'] = True
                result['was_name'] = salida[30]
                result['was_version'] = salida[31]


            matchOS = re.match(r'.*Red\sHat.*elease[:]?\s+([0-9])[.]?.*', result['os_version'])
            if (matchOS):
                if (int(matchOS.group(1)) < REDHAT_MAJOR_VERSION):
                    result['obsolete'] = True
        else:
            result['parsed'] = pipe.returncode
            result['err'] = pipe.stderr.decode()
                    
        return(result)

