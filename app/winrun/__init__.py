from subprocess import STDOUT, CalledProcessError, check_output as qx
import os, json, argparse, sys, time, re, datetime
import socket, subprocess, logging, datetime
import wmi_client_wrapper as wmi
from datetime import datetime


LISTMAPUSER = os.getenv('MAPUSER', 'localhost/_winmap,domain/Administrator').split(',')
LISTMAPPASS = os.getenv('MAPPASS', 'password1,password2').split(',')


if len(LISTMAPUSER) != len(LISTMAPPASS):
    print('MAPUSER and MAPPASS dont have some size of values')
    sys.exit(2)

wmic_commands = {
    #'Win32_OperatingSystem': '''SELECT Caption,FreePhysicalMemory from Win32_OperatingSystem''',
    'Win32_OperatingSystem': '''SELECT InstallDate,Caption,FreePhysicalMemory from Win32_OperatingSystem''',
    'Win32_OperatingSystem_server': '''SELECT ServicePackMajorVersion,LastBootUpTime from Win32_OperatingSystem''',
    'Win32_ComputerSystem': '''SELECT Model,Manufacturer,CurrentTimeZone,DaylightInEffect,EnableDaylightSavingsTime,NumberOfLogicalProcessors,NumberOfProcessors,Status,SystemType,ThermalState,TotalPhysicalMemory,UserName,Name,Domain from Win32_ComputerSystem''',
    'Win32_ComputerSystemProduct': '''SELECT IdentifyingNumber from Win32_ComputerSystemProduct''',
}

wmic_commands2 = {
    'Win32_Processor': '''SELECT Name from Win32_Processor''',
    'Win32_QuickFixEngineering': '''SELECT HotfixID from win32_QuickFixEngineering''',
    'Win32_Product': '''SELECT Name,Version,Vendor,InstallDate from Win32_Product''',
    'Win32_NetworkLoginProfile': '''SELECT LastLogon from Win32_NetworkLoginProfile''',
    'Win32_DiskDrive': '''SELECT Model,Size from Win32_DiskDrive''',
    }

wmic_commands3 = {
    'Win32_OperatingSystem_arch': '''SELECT OSArchitecture from Win32_OperatingSystem''',
}

class Winmaprun():
    def __init__(self, timeout=180):
        self.timeout = timeout
        
    def process_ip(self, ip):

        result = {
            'ip': ip,
            'parsed': 3,
            'err': 'not_analyzed',
            'map_type': 'wintel'
            }

        result = self.get_access(result)
        #print(result)

        if result['parsed'] == 1:
            output_parse = self.wmic_queries(result)
            return output_parse
        else:
            return result


    def get_access(self, result):
        '''
        check access in host.
        - if true, call subproc_exec
        - if false, save the fail status on elasticsearch
        '''

        #print(LISTMAPUSER)
        listpass = 0
        #print(self.timeout)
        for wmi_user in LISTMAPUSER:
            wmiclient = wmi.WmiClientWrapper(username=wmi_user,
                                            password=LISTMAPPASS[listpass],
                                            host=result['ip'])
                                            #timeout=self.timeout)

            #print(wmi_user)
            try:
                #check first access
                output = wmiclient.query(
                    "SELECT CSName from win32_operatingsystem")
                if type(output) is list:
                    result['hostname'] = output[0]['CSName']
                    result['parsed'] = 1
                    result['err'] = 'ready'
                    result['wmiclient']= wmiclient
                    result['wmi_user'] = wmi_user
                    break
            except:
                result['wmiclient']: None

            listpass+=1

        return(result)

    def wmic_queries(self, result):
        #print(result)
        query = result['wmiclient']
        try:
           if 'Win32_Processor' in wmic_commands2:
               output = query.query(wmic_commands2['Win32_Processor'])

               result['ProcName'] = output[0]['Name']
        except:
            pass

        #try:
        #    if 'Win32_Product' in wmic_commands2:
        #        output = query.query(wmic_commands2['Win32_Product'])
        #        producto = []
        #        producto_short = []
        #        product_list = []
        #        #date_epochmillis = int(datetime.datetime.now().strftime("%s"))
        #        #date_epochmillis = int(datetime.now().strftime("%s"))

        #        for i in output:
        #            producto.append(i['Name'] + "=" + i['Version'] + "=" +i['Vendor']+ "=" + i['InstallDate'])
        #            producto_short.append(i['Name'] + "=" + i['Version'])
        #            product_list.append({'Vendor': i['Vendor'],
        #                    'Name': i['Name'],
        #                    'Version': i['Version'],
        #                    'InstallDate' : i['InstallDate'],
        #                    'hostname': result['hostname']
        #                    #'_id': i['Name'] + "-" + result['hostname'] + "-" + str(date_epochmillis)
        #                    })


        #        result['Product_Name'] = producto
        #        result['Product_version'] = producto_short
        #        result['Product_list'] = product_list
        #except:
        #    pass

        try:
            if 'Win32_QuickFixEngineering' in wmic_commands2:
                output = query.query(wmic_commands2['Win32_QuickFixEngineering'])
                fix = []
                for i in output:
                    fix.append(i['HotFixID'])
                result['HotFixID'] = fix
        except:
            pass

        try:
            if 'Win32_DiskDrive' in wmic_commands2:
                output = query.query(wmic_commands2['Win32_DiskDrive'])
                result['hd_model'] = output[0]['Model']
                result['hd_size_gb'] = round(int(output[0]['Size'])/1000000000)
        except:
            pass

        try:
            if 'Win32_NetworkLoginProfile' in wmic_commands2:
                output = query.query(wmic_commands2['Win32_NetworkLoginProfile'])
                login = []
                for i in output:
                    if i['LastLogon'] == None:
                        pass
                    else:
                        if '*' in i['LastLogon']:
                            pass
                        else:
                            date = str(i['LastLogon'].split('.')[0])
                            format_date=datetime.strptime(date, '%Y%m%d%H%M%S').strftime('%Y-%m-%d %H:%M:%S')
                            login.append(str(format_date) +" "+ i['Name'])
                result['last_logon'] = login[:3] 
        except:
            pass

        try:
            for k,v in wmic_commands.items():
                output = query.query(v)
                for a,b in output[0].items():
                    result[a] = b
        except:
            pass

        try:
            if 'Win32_Product' in wmic_commands2:
                output = query.query(wmic_commands2['Win32_Product'])
                producto = []
                producto_short = []
                product_list = []
                #date_epochmillis = int(datetime.datetime.now().strftime("%s"))
                #date_epochmillis = int(datetime.now().strftime("%s"))

                for i in output:
                    producto.append(i['Name'] + "=" + i['Version'] + "=" +i['Vendor']+ "=" + i['InstallDate'])
                    producto_short.append(i['Name'] + "=" + i['Version'])
                    product_list.append({'Vendor': i['Vendor'],
                            'Name': i['Name'],
                            'Version': i['Version'],
                            'InstallDate' : i['InstallDate'],
                            'hostname': result['hostname'],
                            'UserName': result['UserName'],
                            'os_version': result['Caption']
                            #'_id': i['Name'] + "-" + result['hostname'] + "-" + str(date_epochmillis)
                            })


                result['Product_Name'] = producto
                result['Product_version'] = producto_short
                result['Product_list'] = product_list
        except:
            pass



        try:
            output = query.query(wmic_commands3['Win32_OperatingSystem_arch'])
            result['OSArchitecture'] = output[0]['OSArchitecture']
        except:
            pass


        try:
           if 'Win32_Processor' in wmic_commands2:
               output = query.query(wmic_commands2['Win32_Processor'])

               result['ProcName'] = output[0]['Name']
        except:
            pass



        del result['wmiclient']
        try:
            result['os_version'] = result.pop('Caption')
        except:
            pass
        try:
            result['model'] = result.pop('Model')
        except:
            pass
        
        try:
            if 'Manufacturer' in result.keys():
                if result['Manufacturer'] == 'Xen' or result['Manufacturer'] == 'VMware, Inc.':
                    result['server_type'] = 'Virtual'
                else:
                    result['server_type'] = 'Physical'
        except:
            pass


        try:
            date = result['InstallDate'].split('.')[0]
            result['InstallDate'] = datetime.strptime(str(date),'%Y%m%d%H%M%S').strftime('%d-%m-%Y %H:%M:%S')
            result['so_install_date'] = result.pop('InstallDate')
        except:
            pass
        result['parsed'] = 0
        result['err'] = 'analyzed'


        return result
