import threading
import queue
import os
import sshrun
import winrun

THREAD_COUNT = os.getenv('THREAD_COUNT',5)

ssh = sshrun.Sshpassrun()
win= winrun.Winmaprun()

class ApiThread(threading.Thread):
    
    def __init__(self, request_type, q, result):
        threading.Thread.__init__(self)
        self.request_type = request_type
        self.q = q
        self.result = result

    def run(self):
        while True:
            try:
                item = self.q.get()
                if item is None:
                    break
                try:
                    self.result.append(eval(self.request_type + "(item)"))
                finally:
                    self.q.task_done()
            except self.q.Empty:
                pass
    
class Before_Threading():
    def __init__(self, prefix, request_type):
        self.prefix = prefix 
        self.request_type = request_type
        self.result = []
    def init_threading(self):
        q = queue.Queue()
        list_thread = []
        for i in range(THREAD_COUNT):
            t = ApiThread(self.request_type, q, self.result)
            t.daemon = True
            t.start()
            list_thread.append(t)
        
        # add servers in the queue
        for server in self.prefix:
            q.put(server)
        
        # wait for all items to finish processing
        q.join()
        
        # Signal all threads to break and join the main thread
        for x in range(THREAD_COUNT):
            q.put(None)
        for thread in list_thread:
            thread.join()

        return self.result
    
