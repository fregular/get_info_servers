from flask import Blueprint, jsonify, request
import sshrun
import json

sshpass = Blueprint('sshpass', __name__)

ssh = sshrun.Sshpassrun()


@sshpass.route('/sshpass', methods = ['POST'])
def index():
    data = json.loads(request.data)

    if type(data['prefix']) is list:
        result = []
        for server in data['prefix']:
            result.append(ssh.get_access(server))
        return jsonify(result)

    else:
        return jsonify(ssh.get_access(data['prefix']))
    
